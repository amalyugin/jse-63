package ru.t1.malyugin.tm.listener;

import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import ru.t1.malyugin.tm.api.service.ITokenService;
import ru.t1.malyugin.tm.enumerated.Role;
import ru.t1.malyugin.tm.event.ConsoleEvent;

public abstract class AbstractListener {

    @NotNull
    @Autowired
    protected ITokenService tokenService;

    @Nullable
    public abstract String getArgument();

    @NotNull
    public abstract String getName();

    @NotNull
    public abstract String getDescription();

    @NotNull
    public abstract Role[] getRoles();

    @Nullable
    protected String getToken() {
        return tokenService.getToken();
    }

    protected void setToken(@Nullable final String token) {
        tokenService.setToken(token);
    }

    public abstract void handleConsoleEvent(@NotNull ConsoleEvent event);

    @Override
    @NotNull
    public String toString() {
        @NotNull String result = "";
        @NotNull final String name = getName();
        @Nullable final String argument = getArgument();
        @NotNull final String description = getDescription();

        boolean isName = !StringUtils.isBlank(name);
        boolean isArgument = !StringUtils.isBlank(argument);
        boolean isDescription = !StringUtils.isBlank(description);

        result += (isName ? name + (isArgument ? ", " : "") : "");
        result += (isArgument ? argument : "");
        result += (isDescription ? " -> " + description : "");

        return result;
    }

}