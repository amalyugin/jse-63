package ru.t1.malyugin.tm.exception.field;

import org.jetbrains.annotations.NotNull;

public final class NumberIncorrectException extends AbstractFieldException {

    public NumberIncorrectException() {
        super("Error! Number is incorrect...");
    }

    public NumberIncorrectException(@NotNull final String value) {
        super("Error! This number '" + value + "' is incorrect...");
    }

    public NumberIncorrectException(@NotNull final String value, @NotNull final Throwable cause) {
        super("Error! This number '" + value + "' is incorrect...");
    }

}