package ru.t1.malyugin.tm.servlet;

import lombok.SneakyThrows;
import ru.t1.malyugin.tm.enumerated.Status;
import ru.t1.malyugin.tm.model.Project;
import ru.t1.malyugin.tm.model.Task;
import ru.t1.malyugin.tm.repository.ProjectRepository;
import ru.t1.malyugin.tm.repository.TaskRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;

@WebServlet(name = "TaskEdit", value = "/task/edit/*")
public class TaskEditServlet extends HttpServlet {

    @Override
    protected void doGet(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
        final String id = req.getParameter("id");
        final Task task = TaskRepository.getInstance().findById(id);
        final Iterable<Project> projects = ProjectRepository.getInstance().findAll();
        req.setAttribute("task", task);
        req.setAttribute("statuses", Status.values());
        req.setAttribute("projects", projects);
        req.getRequestDispatcher("/WEB-INF/views/task-edit.jsp").forward(req, resp);
    }

    @Override
    @SneakyThrows
    protected void doPost(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
        final String id = req.getParameter("id");
        final String name = req.getParameter("name");
        final String description = req.getParameter("description");
        final String statusValue = req.getParameter("status");
        final Status status = Status.valueOf(statusValue);
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        final String dateStartValue = req.getParameter("start");
        final String dateFinishValue = req.getParameter("finish");
        final String projectId = req.getParameter("projectId");

        final Task task = TaskRepository.getInstance().findById(id);
        task.setName(name);
        task.setDescription(description);
        task.setStatus(status);

        if (!dateStartValue.isEmpty()) task.setStart(simpleDateFormat.parse(dateStartValue));
        if (!dateFinishValue.isEmpty()) task.setFinish(simpleDateFormat.parse(dateFinishValue));
        if (projectId != null && !projectId.isEmpty()) task.setProjectId(projectId);

        TaskRepository.getInstance().save(task);
        resp.sendRedirect("/tasks");
    }
}