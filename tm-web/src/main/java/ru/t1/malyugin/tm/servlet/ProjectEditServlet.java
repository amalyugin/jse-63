package ru.t1.malyugin.tm.servlet;

import lombok.SneakyThrows;
import ru.t1.malyugin.tm.enumerated.Status;
import ru.t1.malyugin.tm.model.Project;
import ru.t1.malyugin.tm.repository.ProjectRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;

@WebServlet(name = "ProjectEdit", value = "/project/edit/*")
public class ProjectEditServlet extends HttpServlet {

    @Override
    protected void doGet(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
        final String id = req.getParameter("id");
        final Project project = ProjectRepository.getInstance().findById(id);
        req.setAttribute("project", project);
        req.setAttribute("statuses", Status.values());
        req.getRequestDispatcher("/WEB-INF/views/project-edit.jsp").forward(req, resp);
    }

    @Override
    @SneakyThrows
    protected void doPost(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
        final String id = req.getParameter("id");
        final String name = req.getParameter("name");
        final String description = req.getParameter("description");
        final String statusValue = req.getParameter("status");
        final Status status = Status.valueOf(statusValue);
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        final String dateStartValue = req.getParameter("start");
        final String dateFinishValue = req.getParameter("finish");

        final Project project = ProjectRepository.getInstance().findById(id);
        project.setName(name);
        project.setDescription(description);
        project.setStatus(status);

        if (!dateStartValue.isEmpty()) project.setStart(simpleDateFormat.parse(dateStartValue));
        if (!dateFinishValue.isEmpty()) project.setFinish(simpleDateFormat.parse(dateFinishValue));

        ProjectRepository.getInstance().save(project);
        resp.sendRedirect("/projects");
    }
}