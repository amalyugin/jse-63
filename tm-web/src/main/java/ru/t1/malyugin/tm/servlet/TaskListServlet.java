package ru.t1.malyugin.tm.servlet;

import ru.t1.malyugin.tm.repository.ProjectRepository;
import ru.t1.malyugin.tm.repository.TaskRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "TaskList", value = "/tasks/*")
public final class TaskListServlet extends HttpServlet {

    @Override
    protected void doGet(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("tasks", TaskRepository.getInstance().findAll());
        req.setAttribute("projectRepository", ProjectRepository.getInstance());
        req.getRequestDispatcher("/WEB-INF/views/task-list.jsp").forward(req, resp);
    }

}