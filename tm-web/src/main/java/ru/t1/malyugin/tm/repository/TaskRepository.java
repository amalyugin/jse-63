package ru.t1.malyugin.tm.repository;

import ru.t1.malyugin.tm.model.Task;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

public final class TaskRepository {

    private static final TaskRepository INSTANCE = new TaskRepository();

    private final Map<String, Task> taskMap = new LinkedHashMap<>();

    {
        create();
        create();
        create();
        create();
    }

    private TaskRepository() {
    }

    public static TaskRepository getInstance() {
        return INSTANCE;
    }

    public void create() {
        add("T - " + ThreadLocalRandom.current().nextInt(0, 1000), "D");
    }

    public void add(final String name, final String description) {
        final Task task = new Task(name, description);
        taskMap.put(task.getId(), task);
    }

    public void save(final Task task) {
        taskMap.put(task.getId(), task);
    }

    public Iterable<Task> findAll() {
        return taskMap.values();
    }

    public Task findById(final String id) {
        return taskMap.get(id);
    }

    public void removeById(final String id) {
        taskMap.remove(id);
    }

}