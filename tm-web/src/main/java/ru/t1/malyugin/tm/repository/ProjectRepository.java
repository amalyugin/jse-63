package ru.t1.malyugin.tm.repository;

import ru.t1.malyugin.tm.model.Project;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

public final class ProjectRepository {

    private static final ProjectRepository INSTANCE = new ProjectRepository();

    private final Map<String, Project> projectMap = new LinkedHashMap<>();

    {
        create();
        create();
        create();
        create();
    }

    private ProjectRepository() {
    }

    public static ProjectRepository getInstance() {
        return INSTANCE;
    }

    public void create() {
        add("P - " + ThreadLocalRandom.current().nextInt(0, 1000), "D");
    }

    public void add(final String name, final String description) {
        final Project project = new Project(name, description);
        projectMap.put(project.getId(), project);
    }

    public void save(final Project project) {
        projectMap.put(project.getId(), project);
    }

    public Iterable<Project> findAll() {
        return projectMap.values();
    }

    public Project findById(final String id) {
        return projectMap.get(id);
    }

    public void removeById(final String id) {
        projectMap.remove(id);
    }

}