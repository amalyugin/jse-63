package ru.t1.malyugin.tm.servlet;

import ru.t1.malyugin.tm.repository.TaskRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "TaskDelete", value = "/task/delete/*")
public class TaskDeleteServlet extends HttpServlet {

    @Override
    protected void doGet(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
        final String id = req.getParameter("id");
        TaskRepository.getInstance().removeById(id);
        resp.sendRedirect("/tasks");
    }

}