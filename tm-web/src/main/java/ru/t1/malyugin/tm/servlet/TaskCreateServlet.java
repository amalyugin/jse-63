package ru.t1.malyugin.tm.servlet;

import ru.t1.malyugin.tm.repository.TaskRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "TaskCreate", value = "/tasks/create/*")
public class TaskCreateServlet extends HttpServlet {

    @Override
    protected void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
        TaskRepository.getInstance().create();
        response.sendRedirect("/tasks");
    }

}