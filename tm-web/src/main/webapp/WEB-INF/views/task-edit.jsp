<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:include page="../include/_header.jsp"/>
<form action="/task/edit?id=${task.id}" method="post">
    <h1>TASK CREATE</h1>
    <fieldset>
        <input type="hidden" name="id" value="${task.id}"/>

        <label for="name">Name:</label>
        <input type="text" name="name" id="name" value="${task.name}"/>

        <label for="description">Description:</label>
        <input type="text" name="description" id="description" value="${task.description}"/>

        <label for="status">Status</label>
        <select id="status" name="status">
            <c:forEach var="status" items="${statuses}">
                <option value="${status}"
                        <c:if test="${task.status == status}">selected</c:if> >${status.displayName}</option>
            </c:forEach>
        </select>

        <label for="project">Project</label>
        <select id="project" name="projectId">
            <option disabled
                    <c:if test="${empty task.projectId}">selected</c:if> value></option>
            <c:forEach var="project" items="${projects}">
                <option value="${project.id}"
                        <c:if test="${task.projectId == project}">selected</c:if> >${project.name}</option>
            </c:forEach>
        </select>

        <label for="start">Start Date:</label>
        <input type="date" name="start" id="start" value=
        <fmt:formatDate pattern="yyyy-MM-dd" value="${task.start}"/>

                <label for="finish">Finish Date:</label>
        <input type="date" name="finish" id="finish" value=
        <fmt:formatDate pattern="yyyy-MM-dd" value="${task.finish}"/>
                </fieldset>
        <button type="reset">RESET</button>
        <button type="submit">SAVE TASK</button>
</form>
<br/>
<jsp:include page="../include/_footer.jsp"/>