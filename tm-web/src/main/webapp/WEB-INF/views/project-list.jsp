<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:include page="../include/_header.jsp"/>
<table border="1">
    <caption>PROJECT LIST</caption>
    <tr>
        <th scope="col">ID</th>
        <th scope="col">NAME</th>
        <th scope="col">DESCRIPTION</th>
        <th scope="col">STATUS</th>
        <th scope="col">START</th>
        <th scope="col">FINISH</th>
        <th scope="col">EDIT</th>
        <th scope="col">DELETE</th>
    </tr>
    <c:forEach var="project" items="${projects}">
        <tr>
            <td><c:out value="${project.id}"/></td>
            <td><c:out value="${project.name}"/></td>
            <td><c:out value="${project.description}"/></td>
            <td><c:out value="${project.status.displayName}"/></td>
            <td><fmt:formatDate pattern="dd.MM.yyyy" value="${project.start}"/></td>
            <td><fmt:formatDate pattern="dd.MM.yyyy" value="${project.finish}"/></td>
            <td><a href="/project/edit/?id=${project.id}"/>EDIT</td>
            <td><a href="/project/delete/?id=${project.id}"/>DELETE</td>
        </tr>
    </c:forEach>
</table>
<br/>
<form action="/projects/create">
    <button>CREATE PROJECT</button>
</form>

<jsp:include page="../include/_footer.jsp"/>