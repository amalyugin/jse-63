<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:include page="../include/_header.jsp"/>
<form action="/project/edit?id=${project.id}" method="post">
    <h1>PROJECT CREATE</h1>
    <fieldset>
        <input type="hidden" name="id" value="${project.id}"/>

        <label for="name">Name:</label>
        <input type="text" name="name" id="name" value="${project.name}"/>

        <label for="description">Description:</label>
        <input type="text" name="description" id="description" value="${project.description}"/>

        <label for="status">Status</label>
        <select id="status" name="status">
            <c:forEach var="status" items="${statuses}">
                <option value="${status}"
                        <c:if test="${project.status == status}">selected</c:if>>${status.displayName}</option>
            </c:forEach>
        </select>

        <label for="start">Start Date:</label>
        <input type="date" name="start" id="start" value=
        <fmt:formatDate pattern="yyyy-MM-dd" value="${project.start}"/>

                <label for="finish">Finish Date:</label>
        <input type="date" name="finish" id="finish" value=
        <fmt:formatDate pattern="yyyy-MM-dd" value="${project.finish}"/>
                </fieldset>
        <button type="reset">RESET</button>
        <button type="submit">SAVE PROJECT</button>
</form>
<br/>
<jsp:include page="../include/_footer.jsp"/>