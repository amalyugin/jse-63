package ru.t1.malyugin.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.malyugin.tm.api.service.dto.ISessionDTOService;
import ru.t1.malyugin.tm.dto.model.SessionDTO;
import ru.t1.malyugin.tm.repository.dto.SessionDTORepository;

@Service
public class SessionDTOService extends AbstractUserOwnedDTOService<SessionDTO> implements ISessionDTOService {

    @NotNull
    private final SessionDTORepository sessionDTORepository;

    @Autowired
    public SessionDTOService(@NotNull final SessionDTORepository sessionDTORepository) {
        this.sessionDTORepository = sessionDTORepository;
    }

    @NotNull
    @Override
    protected SessionDTORepository getRepository() {
        return sessionDTORepository;
    }

}