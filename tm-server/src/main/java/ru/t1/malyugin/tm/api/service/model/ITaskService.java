package ru.t1.malyugin.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.model.Task;

import java.util.List;

public interface ITaskService extends IWBSService<Task> {

    void updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );

    @NotNull
    List<Task> findAllByProjectId(
            @Nullable String userId,
            @Nullable String projectId
    );

    void bindTaskToProject(
            @Nullable String userId,
            @Nullable String taskId,
            @Nullable String projectId
    );

    void unbindTaskFromProject(
            @Nullable String userId,
            @Nullable String taskId,
            @Nullable String projectId
    );

}