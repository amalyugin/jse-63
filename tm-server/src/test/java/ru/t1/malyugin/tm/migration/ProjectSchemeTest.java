package ru.t1.malyugin.tm.migration;

import liquibase.exception.LiquibaseException;
import org.junit.Test;

public class ProjectSchemeTest extends AbstractSchemeTest {

    @Test
    public void projectTest() throws LiquibaseException {
        LIQUIBASE.dropAll();
        LIQUIBASE.update("project");
    }

}